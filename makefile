ASM=nasm
AFLAGS=-f elf64

%.o: %.asm
	$(ASM) $(AFLAGS) -o $@ $<

program: lib.o main.o dict.o
	ld -o program $^

clean:
	rm -f *.o
	rm -f program
	rm -f main

.PHONY:
	clean
