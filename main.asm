%define buffer 256
%include "words.inc"
%include "lib.inc"
extern find_word

global _start

section .data
word_buffer: times 256 db 0

section .rodata
error_text:
    db "Некорректный ввод", 0
not_found_text:
    db "Совпадения не найдены", 0

section .text
_start:
    mov rdi, word_buffer
    mov rsi, 256
    call read_word ; читаем из stdin
    cmp rax, 0
    je .seg_fault ; ничего прочитать не удалось

    mov rdi, word_buffer ; в rdi введенное слово
    mov rsi, NEXT ; в rsi указатель на начало словаря
    push rdx
    call find_word ; ищем
    pop rdx
    cmp rax, 0
    je .not_found

    mov rdi, rax ; адрес начала вхождения в словарь успешно найденного значения
    add rdi, 8 ; адрес самого значения
    inc rdi
    add rdi, rdx
    call print_string
    call exit

.seg_fault:
    mov rdi, error_text
    jmp .print_error_text

.not_found:
    mov rdi, not_found_text
.print_error_text:
    call print_error
.the_end:
    call exit
