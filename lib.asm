global exit
global string_length
global print_string
global print_error
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, rdi
    dec rax
.check:
    inc rax
    cmp BYTE[rax], 0
    jnz .check
    sub rax, rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    mov rdx, rax ; количество байт для записи
    pop rsi
    mov rax, 1 ; номер системного вызова write
    mov rdi, 1 ; номер потока stdout
    mov rsi, rsi ; адрес строки
    mov rdx, rdx ; количество байт для записи
    syscall
    ret

print_error:
    xor rax, rax
    push rdi
    call string_length
    mov rdx, rax ; количество байт для записи
    pop rsi ; адрес строки
    mov rdi, 2 ; номер потока stderr
    mov rax, 1  ; номер системного вызова write
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
print_char:             ; Принимает код символа и выводит его в stdout
    xor rax, rax
    dec rsp
    mov BYTE[rsp], dil
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    inc rsp
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r10, rsp        ; запоминаем состояние rsp в r10
    dec rsp
    mov BYTE[rsp], 0
    mov r8, 10          ; делитель
.divide:
    mov rdx, 0          ; в rsp хранится остаток от деления
    div r8
    add rdx, '0'
    dec rsp
    mov BYTE[rsp], dl
    cmp rax, 0
    jnz .divide
    mov rdi, rsp
    push r10
    call print_string
    pop rsp             ; восстанавливаем состояние rsp
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
.loop:
    mov al, BYTE[rsi]
    cmp BYTE[rdi], al
    jne .finish
    inc rsi
    inc rdi
    cmp BYTE[rsi-1], 0
    jne .loop
    mov rax, 1
    ret
.finish:
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rax, 0 ; номер системного вызова read
    mov rdi, 0 ; номер потока
    mov rsi, rsp ; адрес строки
    mov rdx, 1 ; количество байт для чтения
    syscall
    test rax, rax
    jz .error

    mov al, [rsp]
    jmp .finish
.error:
    xor rax, rax
.finish:
    inc rsp
    ret

; Принимает: адрес начала буфера(rdi), размер буфера(rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx ; счётчик введённых символов

    cmp rsi, 0
    je .error

.loop:
    push rdi
    push rsi
    push rdx

    call read_char

    pop rdx
    pop rsi
    pop rdi

    cmp rax, 0x20
    je .check_start_with
    cmp rax, 0x9
    je .check_start_with
    cmp rax, 0xA
    je .check_start_with
    cmp rax, 0
    je .the_end_check

    jmp .write

.check_start_with:
    cmp rdx, 0
    je .loop
    jmp .success

.write:
    cmp rsi, rdx
    je .error

    mov [rdi + rdx], al

    cmp rax, 0
    je .success

    inc rdx
    jmp .loop
.error:
    xor rax, rax
    xor rdx, rdx
    ret
.the_end_check:
    cmp rdx, 0
    je .error
.success:
    mov byte [rdi + rdx], 0
    mov rax, rdi
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
    mov rcx, 0
    mov rdx, 0

parse_uint_loop:
.loop:
    mov dl, byte[rdi + rcx]
    cmp dl, '0'
    js .finish
    cmp dl, '9'
    ja .finish
    sub dl, '0'
    imul rax, 10
    add rax, rdx
    inc rcx
    jmp .loop
.finish:
    mov rdx, rcx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    mov al, byte [rdi]
    cmp al, '-'
    je .sign
    jmp parse_uint
.sign:
    inc rdi
    call parse_uint
    neg rax
    cmp rdx, 0
    je .error
    inc rdx
    ret
.error:
    xor rax, rax
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Принимает указатель на строку(rdi), указатель на буфер(rsi) и длину буфера(rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
.loop:
    cmp rdx, rcx
    je .fail
    mov al, [rdi + rcx]
    mov [rsi + rcx], al
    inc rcx
    cmp al, 0
    jne .loop
.success:
    mov rax, rcx
    ret
.fail:
    xor rax, rax
    ret
