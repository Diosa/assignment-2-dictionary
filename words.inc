%include "colon.inc"

; Однажды был случай в далёком Макао,
; макака макала коалу в кокао,
; коала лениво лакала какао,
; макака макала, коала икала

section .data

colon "ikala", ikala
db "икала", 0

colon "koala", koala
db "коала", 0

colon "makala", makala
db "макала", 0

colon "makaka", makaka
db "макака", 0
