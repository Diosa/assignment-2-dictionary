global find_word
extern string_equals

section .text

;rdi - указатель на нуль-терминированную строку(значение); rsi - указатель на начало словаря.
;Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.
find_word:
    push rdi
    push rsi
    add rsi, 8 ; кладём в rsi указатель на значение
    call string_equals
    pop rsi ; извлекаем указатель на начало вхождения (не на значение)
    pop rdi
    cmp rax, 1 ; проверяем, что значение соответствует искомому
    je .success ; в rsi лежит уже нужный указатель

    mov rsi, [rsi] ; кладём в rsi указатель на начало вхождения следующего элемента
    cmp rsi, 0 ; проверяем, дошли ли до конца

    jne find_word
.end:
    xor rax, rax
    ret
.success:
    mov rax, rsi
    ret
