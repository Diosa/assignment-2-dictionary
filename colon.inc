%define NEXT 0

%macro colon 2;
    %ifid %2
        %2: dq NEXT
        %define NEXT %2
    %else
        %error "в качестве второго параметра должен быть передан идентификатор"
    %endif

    %ifstr %1
        db %1, 0
    %else
        %error "первый параметр должен быть строкой"
    %endif

%endmacro
